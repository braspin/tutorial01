#pragma once
#include "event_manager.h"
#include <SFML/Window/Event.hpp>

namespace tutorial01 {

	class keyboard {

		typedef boost::signals2::signal< void(const sf::Event::KeyEvent&) > a_pressed_type;
		typedef boost::signals2::signal< void(const sf::Event::KeyEvent&) > s_pressed_type;
		typedef boost::signals2::signal< void(const sf::Event::KeyEvent&) > d_pressed_type;
		typedef boost::signals2::signal< void(const sf::Event::KeyEvent&) > w_pressed_type;

	public:
		keyboard(tutorial01::event_manager&);

	public:
		void a_pressed_slot(const a_pressed_type::slot_type&);
		void s_pressed_slot(const s_pressed_type::slot_type&);
		void d_pressed_slot(const d_pressed_type::slot_type&);
		void w_pressed_slot(const w_pressed_type::slot_type&);

	public SIGNAL:
		void on_a_pressed(const sf::Event::KeyEvent&);
		void on_s_pressed(const sf::Event::KeyEvent&);
		void on_d_pressed(const sf::Event::KeyEvent&);
		void on_w_pressed(const sf::Event::KeyEvent&);
		
	public SLOT:
		void on_key_pressed(const sf::Event::KeyEvent&);

	private:
		a_pressed_type _on_a_pressed;
		s_pressed_type _on_s_pressed;
		d_pressed_type _on_d_pressed;
		w_pressed_type _on_w_pressed;

	};
}