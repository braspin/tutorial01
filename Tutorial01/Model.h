#pragma once
#include <vector>

using namespace std;

class Model
{
public:
	Model();
	vector<Vertex> vertices;
	vector<FaceTexture> faces;
	vector<TextureCoordinate> textureCoordinates;
	vector<Vertex> normals;
	void loadModel(const char *fileName);
	void setupMesh();
	void draw();
private:
	GLuint VAO, VBO;
};