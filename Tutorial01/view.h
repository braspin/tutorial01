#pragma once
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/View.hpp>

#include "mouse.h"
#include "map.h"

namespace tutorial01 {
	class view : public sf::View {

		public:
			view(tutorial01::mouse&, tutorial01::map&);

		public SLOT:
			void zoom_in(const sf::Event::MouseWheelScrollEvent&);
			void zoom_out(const sf::Event::MouseWheelScrollEvent&);

		private:
			tutorial01::map *_map;
			sf::FloatRect _rect;

	};
}