#include "view.h"
#include <boost/lexical_cast.hpp>

tutorial01::view::view(tutorial01::mouse& m, tutorial01::map& d)
{
	_map = &d;
	reset(sf::FloatRect((_map->width()/2)-(1024/2), (_map->height()/2)-(728/2), 1024, 728));
	setSize(sf::Vector2f(1024, 728));
	m.scrolled_up_slot(boost::bind(&tutorial01::view::zoom_in, this, _1));
	m.scrolled_down_slot(boost::bind(&tutorial01::view::zoom_out, this, _1));
}

void tutorial01::view::zoom_in(const sf::Event::MouseWheelScrollEvent &m)
{
	if(getSize().x < _map->width_limit()){
		zoom(1.05f);
	}
}


void tutorial01::view::zoom_out(const sf::Event::MouseWheelScrollEvent &m)
{
	if (getSize().x > _map->width_limit()/3) {
		zoom(0.95f);
	}
}
