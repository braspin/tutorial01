#pragma once
#include "keyboard.h"
#include <SFML/Graphics.hpp>

namespace tutorial01 {
	class map : public sf::Drawable, public sf::Transformable
	{

	public:
		map(tutorial01::keyboard&);

		bool load(const std::string& , sf::Vector2u , const int* , unsigned int , unsigned int );


		int height();
		int height_limit();

		int width();
		int width_limit();
	public SLOT:
		void up(const sf::Event::KeyEvent&);
		void down(const sf::Event::KeyEvent&);
		void left(const sf::Event::KeyEvent&);
		void right(const sf::Event::KeyEvent&);

	private:
		virtual void draw(sf::RenderTarget&, sf::RenderStates) const;

	private:
		sf::VertexArray _vertices;
		sf::Texture _tileset;
		int _height;
		int _width;
		int _height_limit;
		int _width_limit;
	};
}