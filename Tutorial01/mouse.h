#pragma once
#include "event_manager.h"
#include <SFML/Window/Event.hpp>

namespace tutorial01 {

	class mouse {

		typedef boost::signals2::signal< void(const sf::Event::MouseWheelScrollEvent&) > scrolled_type;

	public:
		mouse(tutorial01::event_manager&);

	public:
		void scrolled_up_slot(const scrolled_type::slot_type&);
		void scrolled_down_slot(const scrolled_type::slot_type&);

	public SIGNAL:
		void on_scrolled_up(const sf::Event::MouseWheelScrollEvent&);
		void on_scrolled_down(const sf::Event::MouseWheelScrollEvent&);

	public SLOT:
		void on_pressed(const sf::Event::MouseButtonEvent&);
		void on_scrolled(const sf::Event::MouseWheelScrollEvent&);

	private:
		scrolled_type _on_scrolled_up;
		scrolled_type _on_scrolled_down;
	};
}