#pragma once
#include <boost/signals2.hpp>
#include <SFML/Window/Event.hpp>

#include "core.h"

namespace tutorial01 {

	class event_manager {
		
		typedef boost::signals2::signal< void(const sf::Event::KeyEvent&) > key_pressed_type;
		typedef boost::signals2::signal< void(const sf::Event::MouseButtonEvent&) > mouse_pressed_type;
		typedef boost::signals2::signal< void(const sf::Event::MouseWheelScrollEvent&) > mouse_scrolled_type;

		public:
			event_manager();

		public:
			void key_pressed_slot(const key_pressed_type::slot_type&);
			void mouse_pressed_slot(const mouse_pressed_type::slot_type&);
			void mouse_scrolled_slot(const mouse_scrolled_type::slot_type&);
	
		public SIGNAL:
			void on_key_pressed(const sf::Event::KeyEvent&);
			void on_mouse_pressed(const sf::Event::MouseButtonEvent&);
			void on_mouse_scrolled(const sf::Event::MouseWheelScrollEvent&);

		public SLOT:
			void on_event(const sf::Event&);

		private:
			key_pressed_type    _on_key_pressed;
			mouse_pressed_type  _on_mouse_pressed;
			mouse_scrolled_type _on_mouse_scrolled;
			
	};
}