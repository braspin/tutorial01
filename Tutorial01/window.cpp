#include <Windows.h>
#include <SFML/Window/Event.hpp>

#include "window.h"

tutorial01::window::window(sf::VideoMode mode, const sf::String & title, sf::Uint32 style, const sf::ContextSettings & settings) : sf::RenderWindow(mode, title, style, settings)
{

}

bool tutorial01::window::pullEvent(sf::Event& event)
{
	if (pollEvent(event)) {
		return true;
	} else {

		event.key.alt = HIWORD(GetAsyncKeyState(VK_MENU)) != 0;
		event.key.control = HIWORD(GetAsyncKeyState(VK_CONTROL)) != 0;
		event.key.shift = HIWORD(GetAsyncKeyState(VK_SHIFT)) != 0;
		event.key.system = HIWORD(GetAsyncKeyState(VK_LWIN)) || HIWORD(GetAsyncKeyState(VK_RWIN));

		if (GetKeyState('A')) {
			event.type = sf::Event::KeyPressed;
			event.key.code = sf::Keyboard::A;
			return true;
		}
		else if (GetKeyState('S')) {
			event.type = sf::Event::KeyPressed;
			event.key.code = sf::Keyboard::S;
			return true;
		}
		else if (GetKeyState('D')) {
			event.type = sf::Event::KeyPressed;
			event.key.code = sf::Keyboard::D;
			return true;
		}
		else if (GetKeyState('W')) {
			event.type = sf::Event::KeyPressed;
			event.key.code = sf::Keyboard::W;
			return true;
		}
		else {
			memset(&event, 0, sizeof(sf::Event));
			return false;
		}
	}
}
