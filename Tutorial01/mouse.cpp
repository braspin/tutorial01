#include "mouse.h"

tutorial01::mouse::mouse(tutorial01::event_manager& e)
{
	e.mouse_pressed_slot(boost::bind(&tutorial01::mouse::on_pressed, this, _1));
	e.mouse_scrolled_slot(boost::bind(&tutorial01::mouse::on_scrolled, this, _1));
}

void tutorial01::mouse::scrolled_up_slot(const scrolled_type::slot_type& slot)
{
	_on_scrolled_up.connect(slot);
}

void tutorial01::mouse::scrolled_down_slot(const scrolled_type::slot_type& slot)
{
	_on_scrolled_down.connect(slot);
}

void tutorial01::mouse::on_scrolled_up(const sf::Event::MouseWheelScrollEvent &m)
{
	_on_scrolled_up(m);
}

void tutorial01::mouse::on_scrolled_down(const sf::Event::MouseWheelScrollEvent &m)
{
	_on_scrolled_down(m);
}

void tutorial01::mouse::on_pressed(const sf::Event::MouseButtonEvent& m)
{

}

void tutorial01::mouse::on_scrolled(const sf::Event::MouseWheelScrollEvent& m)
{
	if (m.delta > 0) {
		on_scrolled_up(m);
	}
	else {
		on_scrolled_down(m);
	}
}
