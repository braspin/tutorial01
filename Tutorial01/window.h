#pragma once
#include <SFML/Graphics/RenderWindow.hpp>

namespace tutorial01 {
	
	class window : public sf::RenderWindow {
		public:
			window(sf::VideoMode mode, const sf::String& title, sf::Uint32 style = sf::Style::Default, const sf::ContextSettings& settings = sf::ContextSettings());
	
		public:
			bool pullEvent(sf::Event& event);
	};
}