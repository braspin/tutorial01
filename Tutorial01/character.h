#pragma once
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>

#include "view.h"
#include "keyboard.h"
#include "mouse.h"

struct walk {
	sf::IntRect begin() {
		return sf::IntRect(0, 0, 65, 115);
	}

	sf::IntRect& next() {
		_iterator = (++_iterator != _sprite.end() ? _iterator : _sprite.begin());
		return *_iterator;
	}

	sf::IntRect& same() {
		return *_iterator;
	}

	std::vector<sf::IntRect> _sprite;
	std::vector<sf::IntRect>::iterator _iterator;
};

struct walk_down : public walk {
	walk_down() {
		_sprite.push_back(sf::IntRect(65, 0, 60, 115));
		_sprite.push_back(sf::IntRect(140, 0, 60, 115));
		_iterator = _sprite.begin();
	}
};

struct walk_up : public walk {
	walk_up() {
		_sprite.push_back(sf::IntRect(0, 370, 60, 115));
		_sprite.push_back(sf::IntRect(65, 370, 60, 115));
		_sprite.push_back(sf::IntRect(140, 370, 60, 115));
		_iterator = _sprite.begin();
	}
};

struct walk_left : public walk {
	walk_left() {
		_sprite.push_back(sf::IntRect(0, 120, 70, 125));
		_sprite.push_back(sf::IntRect(65, 120, 70, 125));
		_sprite.push_back(sf::IntRect(140, 120, 70, 125));
		_iterator = _sprite.begin();
	}
};

struct walk_right : public walk {
	walk_right() {
		_sprite.push_back(sf::IntRect(0, 240, 70, 125));
		_sprite.push_back(sf::IntRect(70, 240, 70, 125));
		_sprite.push_back(sf::IntRect(140, 240, 70, 125));
		_iterator = _sprite.begin();
	}
};

namespace tutorial01 {
	class character {
		public:
			character(const std::string& file, const tutorial01::view&, tutorial01::keyboard&, tutorial01::mouse&);

		public SLOT:
			void up(const sf::Event::KeyEvent&);
			void down(const sf::Event::KeyEvent&);
			void left(const sf::Event::KeyEvent&);
			void right(const sf::Event::KeyEvent&);

		public:
			sf::Sprite& draw();

		private:
			std::string _file;
			sf::Sprite  _sprite;
			sf::Texture _texture;

		private:
			sf::Clock  _clock;
			walk_down  _walk_down;
			walk_left  _walk_left;
			walk_up    _walk_up;
			walk_right _walk_right;
	};
 }