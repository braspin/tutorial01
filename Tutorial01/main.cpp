#pragma once

#include <SFML/Graphics.hpp>

#include "event_manager.h"
#include "keyboard.h"
#include "map.h"
#include "mouse.h"
#include "view.h"
#include "window.h"
#include "character.h"

int main()
{
	tutorial01::event_manager e;
	tutorial01::keyboard k(e);
	tutorial01::mouse m(e);
	
	sf::Event event;

	boost::signals2::signal<void(void)> x;
	tutorial01::window window(sf::VideoMode(1024, 768), "SFML works!");

	const int level[] =
	{
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
		120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120, 120,
	};

	// create the tilemap from the level definition
	tutorial01::map background(k);
	if (!background.load("media/plain.png", sf::Vector2u(256, 256), level, 23, 16)) {
		return -1;
	}

	tutorial01::view v(m, background);

	tutorial01::character c("media/person.png", v, k, m);

	sf::Texture texture2;
	if (!texture2.loadFromFile("media/forest13.png"))
	{
		return -1;
	}

	sf::Sprite sprite2;
	sprite2.setTexture(texture2);
	//sprite2.setPosition(center);

	window.setKeyRepeatEnabled(false);
	window.setFramerateLimit(60);
	while (window.isOpen())
	{
		
		if (window.pullEvent(event))
		{
			if (sf::Event::Closed == event.type) {
				window.close();
			}
			else {
				e.on_event(event);
			}
		}
	
		window.setView(v);
		window.clear();
		window.draw(background);	
		window.draw(c.draw());
		window.draw(sprite2);
		window.display();
	}

	return 0;
}