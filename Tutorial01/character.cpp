#include "character.h"
#include <boost/lexical_cast.hpp>


tutorial01::character::character(const std::string& file, const tutorial01::view& v, tutorial01::keyboard& k, tutorial01::mouse& m)
{
	_file = file;

	if (!_texture.loadFromFile("media/person.png"))
	{
		system(0);
	}

	_sprite.setTexture(_texture);
	_sprite.setTextureRect(_walk_down.begin());
	auto center = v.getCenter();
	center.y -= 105;
	center.x -= 64;
	_sprite.setPosition(center);

	k.a_pressed_slot(boost::bind(&tutorial01::character::left, this, _1));
	k.s_pressed_slot(boost::bind(&tutorial01::character::down, this, _1));
	k.d_pressed_slot(boost::bind(&tutorial01::character::right, this, _1));
	k.w_pressed_slot(boost::bind(&tutorial01::character::up, this, _1));
}

void tutorial01::character::up(const sf::Event::KeyEvent& k)
{
	if (_clock.getElapsedTime().asMilliseconds() > 150) {
		_sprite.setTextureRect(_walk_up.next());
		_clock.restart();
	}
}

void tutorial01::character::down(const sf::Event::KeyEvent& k)
{
	if (_clock.getElapsedTime().asMilliseconds() > 150) {
		_sprite.setTextureRect(_walk_down.next());
		_clock.restart();
	}
}

void tutorial01::character::left(const sf::Event::KeyEvent& k)
{
	if (_clock.getElapsedTime().asMilliseconds() > 150) {
		_sprite.setTextureRect(_walk_left.next());
		_clock.restart();
	}
}

void tutorial01::character::right(const sf::Event::KeyEvent& k)
{
	if (_clock.getElapsedTime().asMilliseconds() > 150) {
		_sprite.setTextureRect(_walk_right.next());
		_clock.restart();
	}
}

sf::Sprite& tutorial01::character::draw()
{
	return _sprite;
}
