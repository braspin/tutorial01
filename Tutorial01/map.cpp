#include "map.h"

tutorial01::map::map(tutorial01::keyboard& k)
{
	k.a_pressed_slot(boost::bind(&tutorial01::map::left, this, _1));
	k.s_pressed_slot(boost::bind(&tutorial01::map::down, this, _1));
	k.d_pressed_slot(boost::bind(&tutorial01::map::right, this, _1));
	k.w_pressed_slot(boost::bind(&tutorial01::map::up, this, _1));
}

bool tutorial01::map::load(const std::string& tileset, sf::Vector2u tileSize, const int* tiles, unsigned int width, unsigned int height)
{
	// load the tileset texture
	if (!_tileset.loadFromFile(tileset))
		return false;

	
	_width = tileSize.x * width;
	_width_limit = _width /2;
	_height = tileSize.y * height;
	_height_limit = _height/2;

	// resize the vertex array to fit the level size
	_vertices.setPrimitiveType(sf::Quads);
	_vertices.resize(width * height * 4);

	// populate the vertex array, with one quad per tile
	for (unsigned int i = 0; i < width; ++i)
		for (unsigned int j = 0; j < height; ++j)
		{
			// get the current tile number
			int tileNumber = tiles[i + j * width];

			// find its position in the tileset texture
			int tu = tileNumber % (_tileset.getSize().x / tileSize.x);
			int tv = tileNumber / (_tileset.getSize().x / tileSize.x);

			// get a pointer to the current tile's quad
			sf::Vertex* quad = &_vertices[(i + j * width) * 4];

			// define its 4 corners
			quad[0].position = sf::Vector2f(i * tileSize.x, j * tileSize.y);
			quad[1].position = sf::Vector2f((i + 1) * tileSize.x, j * tileSize.y);
			quad[2].position = sf::Vector2f((i + 1) * tileSize.x, (j + 1) * tileSize.y);
			quad[3].position = sf::Vector2f(i * tileSize.x, (j + 1) * tileSize.y);

			// define its 4 texture coordinates
			quad[0].texCoords = sf::Vector2f(tu * tileSize.x, tv * tileSize.y);
			quad[1].texCoords = sf::Vector2f((tu + 1) * tileSize.x, tv * tileSize.y);
			quad[2].texCoords = sf::Vector2f((tu + 1) * tileSize.x, (tv + 1) * tileSize.y);
			quad[3].texCoords = sf::Vector2f(tu * tileSize.x, (tv + 1) * tileSize.y);
		}

	return true;
}

int tutorial01::map::height()
{
	return _height;
}

int tutorial01::map::height_limit()
{
	return _height_limit;
}

int tutorial01::map::width()
{
	return _width;
}

int tutorial01::map::width_limit()
{
	return _width_limit;
}

void tutorial01::map::up(const sf::Event::KeyEvent& k)
{
	auto position = this->getPosition();
	if (k.shift == true) {
		position.y += 15;
	}
	else {
		position.y += 10;
	}
	this->setPosition(position);
}

void tutorial01::map::down(const sf::Event::KeyEvent& k)
{
	auto position = this->getPosition();
	if (k.shift == true) {
		position.y -= 15;
	}
	else {
		position.y -= 10;
	}
	this->setPosition(position);
}

void tutorial01::map::left(const sf::Event::KeyEvent& k)
{
	auto position = this->getPosition();
	if (k.shift == true) {
		position.x += 15;
	}
	else {
		position.x += 10;
	}
	this->setPosition(position);
}

void tutorial01::map::right(const sf::Event::KeyEvent& k)
{
	auto position = this->getPosition();
	if (k.shift == true) {
		position.x -= 15;
	}
	else {
		position.x -= 10;
	}
	this->setPosition(position);
}

void tutorial01::map::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	// apply the transform
	states.transform *= getTransform();
	
	// apply the tileset texture
	states.texture = &_tileset;
	
	// draw the vertex array
	target.draw(_vertices, states);
}