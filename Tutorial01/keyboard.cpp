#include "keyboard.h"
#include <boost/lexical_cast.hpp>

tutorial01::keyboard::keyboard(tutorial01::event_manager& e)
{
	e.key_pressed_slot(boost::bind(&tutorial01::keyboard::on_key_pressed, this, _1));
}

void tutorial01::keyboard::a_pressed_slot(const a_pressed_type::slot_type& slot)
{
	_on_a_pressed.connect(slot);
}

void tutorial01::keyboard::s_pressed_slot(const s_pressed_type::slot_type& slot)
{
	_on_s_pressed.connect(slot);
}

void tutorial01::keyboard::d_pressed_slot(const d_pressed_type::slot_type& slot)
{
	_on_d_pressed.connect(slot);
}

void tutorial01::keyboard::w_pressed_slot(const w_pressed_type::slot_type &slot)
{
	_on_w_pressed.connect(slot);
}

void tutorial01::keyboard::on_key_pressed(const sf::Event::KeyEvent& k)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
		on_a_pressed(k);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
		on_s_pressed(k);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
		on_d_pressed(k);
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
		on_w_pressed(k);
	}
}

void tutorial01::keyboard::on_a_pressed(const sf::Event::KeyEvent& k) {
	_on_a_pressed(k);
}

void tutorial01::keyboard::on_s_pressed(const sf::Event::KeyEvent& k)
{
	_on_s_pressed(k);
}

void tutorial01::keyboard::on_d_pressed(const sf::Event::KeyEvent& k)
{
	_on_d_pressed(k);
}

void tutorial01::keyboard::on_w_pressed(const sf::Event::KeyEvent& k)
{
	_on_w_pressed(k);
}
