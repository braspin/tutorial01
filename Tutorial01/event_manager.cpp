#include "event_manager.h"
#include <boost/lexical_cast.hpp>

tutorial01::event_manager::event_manager()
{
	
}

void tutorial01::event_manager::key_pressed_slot(const key_pressed_type::slot_type & slot)
{
	_on_key_pressed.connect(slot);
}

void tutorial01::event_manager::mouse_pressed_slot(const mouse_pressed_type::slot_type & slot)
{
	_on_mouse_pressed.connect(slot);
}

void tutorial01::event_manager::mouse_scrolled_slot(const mouse_scrolled_type::slot_type & slot)
{
	_on_mouse_scrolled.connect(slot);
}

void tutorial01::event_manager::on_key_pressed(const sf::Event::KeyEvent& k) {
	_on_key_pressed(k);
}

void tutorial01::event_manager::on_mouse_pressed(const sf::Event::MouseButtonEvent& m)
{
	_on_mouse_pressed(m);
}

void tutorial01::event_manager::on_mouse_scrolled(const sf::Event::MouseWheelScrollEvent& m)
{
	_on_mouse_scrolled(m);
}



void tutorial01::event_manager::on_event(const sf::Event& e)
{
	switch (e.type)
	{
		// window closed
	case sf::Event::Closed:
		//window.close();
		break;
		// key pressed
	case sf::Event::KeyPressed:
		on_key_pressed(e.key);
		break;
	case sf::Event::MouseButtonPressed:
		on_mouse_pressed(e.mouseButton);
		break;
	case sf::Event::MouseWheelScrolled:
		on_mouse_scrolled(e.mouseWheelScroll);
		break;
	default:
		break;
	}
}




